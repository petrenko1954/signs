require_relative 'boot'

require 'rails/all'
#Sept_11
#config.action_controller.permit_all_parameters = true
#/Sept_11
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module H263
  class Application < Rails::Application

#Sept_11
config.action_controller.permit_all_parameters = true
#/Sept_11



    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
#August-09
    config.action_view.embed_authenticity_token_in_remote_forms = true


  end
end
