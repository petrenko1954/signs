# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_29_092602) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "TblCourse", id: false, force: :cascade do |t|
    t.integer "facID"
    t.integer "CourseID"
    t.string "Course", limit: 50
    t.integer "Nabor", limit: 2
    t.integer "StudVsego"
  end

  create_table "TblFac", id: false, force: :cascade do |t|
    t.integer "universityid"
    t.integer "FacID", null: false
    t.string "Fac", limit: 100
    t.string "Faculty", limit: 50
  end

  create_table "TblGroup", id: false, force: :cascade do |t|
    t.integer "courseid"
    t.integer "prepNid"
    t.integer "yearid"
    t.integer "dateid"
    t.integer "GroupID", null: false
    t.integer "dateStrtid"
    t.integer "GroupID2016"
    t.string "Otdelenie", limit: 50
    t.integer "PrepFIOid"
    t.integer "Group"
    t.string "GroupNazv", limit: 20
    t.integer "ClID"
    t.float "ReitingGr"
    t.string "Language", limit: 1
  end

  create_table "TblModul", id: false, force: :cascade do |t|
    t.integer "ModuleID"
    t.integer "departID"
    t.integer "kafedraid"
    t.string "Module", limit: 100
    t.integer "nObzorid"
    t.integer "N"
    t.integer "Min"
    t.integer "Max"
    t.integer "Mogl"
    t.string "ModuleAlias", limit: 50
  end

  create_table "TblStudAllowed", id: false, force: :cascade do |t|
    t.integer "studentid"
    t.integer "StudAllowedID"
    t.datetime "StudAllowedDate"
    t.string "StudAllowedText", limit: 50
    t.string "StudAllowedLog", limit: 1
  end

  create_table "TblStudent", id: false, force: :cascade do |t|
    t.integer "groupid_new"
    t.integer "groupid_Alias"
    t.integer "groupid"
    t.integer "StudentID", null: false
    t.string "country", limit: 50
    t.string "FIO", limit: 150
    t.string "LastName", limit: 150
    t.string "FirstName", limit: 50
    t.string "Allowed", limit: 50
    t.string "NotAllowed", limit: 50
    t.datetime "DateAllowed"
    t.string "Present", limit: 50
    t.string "Signed", limit: 50
    t.integer "ECTS"
    t.string "CreditBook", limit: 50
    t.string "CreditBookSigned", limit: 50
    t.string "eMail", limit: 50
    t.string "Name Short", limit: 50
    t.string "Nickname", limit: 50
    t.string "Phone", limit: 50
    t.string "I", limit: 50
    t.string "O", limit: 50
    t.string "Imia", limit: 50
    t.string "Otch", limit: 50
    t.integer "'Parole"
    t.string "Parole", limit: 50
    t.datetime "DateRefresh"
    t.integer "OnOff"
    t.integer "Primitca1"
    t.integer "Primitca2"
    t.string "IP", limit: 50
    t.integer "DomWAN"
    t.datetime "Checked"
    t.decimal "Mark", precision: 18
    t.string "Balls", limit: 50
  end

  create_table "active_admin_comments", id: :serial, force: :cascade do |t|
    t.string "namespace", limit: 255
    t.text "body"
    t.string "resource_id", limit: 255, null: false
    t.string "resource_type", limit: 255, null: false
    t.integer "author_id"
    t.string "author_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", id: :serial, force: :cascade do |t|
    t.string "email", limit: 255, default: "", null: false
    t.string "encrypted_password", limit: 255, default: "", null: false
    t.string "reset_password_token", limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "article_files", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "file_file_name", limit: 255
    t.string "file_content_type", limit: 255
    t.integer "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "articles", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.integer "author_id"
    t.string "year", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "journal_id"
    t.string "coauthors", limit: 255
    t.string "publication_data", limit: 255
    t.string "status"
    t.index ["status"], name: "index_articles_on_status"
  end

  create_table "articles_chapters", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "chapter_id"
  end

  create_table "articles_reviews", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "review_id"
  end

  create_table "articles_sections", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "section_id"
  end

  create_table "articles_subsections", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "subsection_id"
  end

  create_table "assgns", force: :cascade do |t|
    t.text "rolename"
    t.integer "user_1d"
    t.integer "role_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id", "created_at"], name: "index_assgns_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_assgns_on_user_id"
  end

  create_table "assings", id: :bigint, default: -> { "nextval('assigns_id_seq'::regclass)" }, force: :cascade do |t|
    t.text "rolename"
    t.integer "user_1d"
    t.integer "role_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id", "created_at"], name: "index_assigns_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_assigns_on_user_id"
  end

  create_table "authors", id: :serial, force: :cascade do |t|
    t.string "first_name", limit: 255
    t.string "last_name", limit: 255
    t.string "patronymic", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bookmarks", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.text "link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "folder_id"
  end

  create_table "chapters", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.string "commenter", limit: 255
    t.text "body"
    t.integer "micropost_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "article_id"
    t.index ["article_id"], name: "index_comments_on_article_id"
    t.index ["micropost_id"], name: "index_comments_on_micropost_id"
  end

  create_table "conferences", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.text "full_title"
    t.string "place", limit: 255
    t.integer "year"
    t.integer "month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "conferences_users", id: false, force: :cascade do |t|
    t.integer "conference_id"
    t.integer "user_id"
    t.index ["conference_id", "user_id"], name: "index_conferences_users_on_conference_id_and_user_id", unique: true
  end

  create_table "folders", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.integer "folder_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "folders_users", id: :serial, force: :cascade do |t|
    t.integer "folder_id"
    t.integer "user_id"
  end

  create_table "journals", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "microposts", id: :serial, force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "picture", limit: 255
    t.index ["user_id", "created_at"], name: "index_microposts_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_microposts_on_user_id"
  end

  create_table "old_articles", id: :serial, force: :cascade do |t|
    t.integer "author_idi"
    t.integer "article_idi"
    t.integer "article_id_old"
    t.string "coauthors", limit: 255
    t.string "journal", limit: 255
    t.string "nazvanie", limit: 255
    t.string "year", limit: 255
    t.string "shapka1", limit: 255
    t.string "shapka2", limit: 255
    t.string "month_tp", limit: 255
    t.string "que", limit: 255
    t.boolean "vpechati"
    t.string "status", limit: 255
    t.string "type_art", limit: 255
    t.string "http_address", limit: 255
    t.string "person", limit: 255
    t.string "word_abs", limit: 255
    t.string "word_abstr", limit: 255
    t.string "pdf_abstr", limit: 255
    t.string "word", limit: 255
    t.string "word2", limit: 255
    t.string "pdf", limit: 255
    t.string "browser_file", limit: 255
    t.string "packet", limit: 255
    t.string "transl", limit: 255
    t.date "checked_up"
    t.text "resume"
    t.boolean "ref"
    t.boolean "xerox"
    t.text "mistake"
    t.string "email", limit: 255
    t.string "file", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_authors", id: :serial, force: :cascade do |t|
    t.integer "author_idi"
    t.integer "old_author_idi"
    t.string "author_name", limit: 255
    t.string "short_i", limit: 255
    t.string "short_o", limit: 255
    t.string "imya", limit: 255
    t.string "otch", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_level4s", id: :serial, force: :cascade do |t|
    t.integer "punkt_idi"
    t.integer "level_4_idi"
    t.string "level_4", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_obzors", id: :serial, force: :cascade do |t|
    t.integer "n_from_razdel"
    t.integer "n_obzor"
    t.string "obzor_podst", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position1s", id: :serial, force: :cascade do |t|
    t.integer "article_idi"
    t.integer "position1_idi"
    t.string "position1", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position2s", id: :serial, force: :cascade do |t|
    t.integer "position1_idi"
    t.integer "position2_idi"
    t.string "position2", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position3s", id: :serial, force: :cascade do |t|
    t.integer "position2_idi"
    t.integer "position3_idi"
    t.string "position3", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_position4s", id: :serial, force: :cascade do |t|
    t.integer "position3_idi"
    t.integer "position4_idi"
    t.string "position4", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_punkts", id: :serial, force: :cascade do |t|
    t.integer "n_from_obzor"
    t.integer "n_punct"
    t.string "punct_podst", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "old_razdels", id: :serial, force: :cascade do |t|
    t.integer "n_razdel"
    t.string "razdel_podst", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "relationships", id: :serial, force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "reports", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "year"
    t.integer "month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reviews", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255
    t.integer "chapter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sections", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, default: "title is missing"
    t.integer "review_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "signs", force: :cascade do |t|
    t.text "rolename"
    t.integer "rolepodst_id"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id", "rolepodst_id"], name: "index_signs_on_user_id_and_rolepodst_id", unique: true
    t.index ["user_id"], name: "index_signs_on_user_id"
  end

  create_table "students", force: :cascade do |t|
    t.bigint "sign_id"
    t.string "country"
    t.string "fio"
    t.string "lastname"
    t.string "firstname"
    t.string "email"
    t.string "comment"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sign_id"], name: "index_students_on_sign_id"
  end

  create_table "subsections", id: :serial, force: :cascade do |t|
    t.string "title", limit: 255, default: "title is missing"
    t.integer "section_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
  end

  create_table "users_old", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "id_old"
    t.string "name", limit: 255
    t.string "email", limit: 255
    t.string "login", limit: 255
    t.string "phone", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "remember_digest", limit: 255
    t.string "admin", limit: 5
    t.string "activation_digest", limit: 255
    t.string "activated", limit: 5
    t.datetime "activated_at"
    t.string "password_digest", limit: 255
    t.string "encrypted_password", limit: 255
  end

  add_foreign_key "students", "signs"
end
