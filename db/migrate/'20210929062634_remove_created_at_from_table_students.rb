class RemoveCreatedAtFromTableStudents < ActiveRecord::Migration[6.0]
  def change

    remove_column :students, :field_name, :created_at
  end
end
