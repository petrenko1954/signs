class AddIndexToSignsUserRolepodst < ActiveRecord::Migration[6.0]
  def change
add_index :signs, [:user_id, :rolepodst_id], unique: true
  end
end
