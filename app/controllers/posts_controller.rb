class PostsController < InheritedResources::Base
before_action :logged_in_user, only: [:create, :destroy]
def edit
 @post = Post.find(params[:id]) 
end



  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end

 def create
  @student = Student.find(params[:student_id])
#@post = Post.find(params[:post_id])
 @post = @student.posts.create(post_params)

     @student.user_id = current_user.id
 
       

 if @post.save
      flash[:success] = "post created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
end


  private

    def post_params
      params.require(:post).permit(:user_id, :sign_id, :content, :student_id)
    end

end
